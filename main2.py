import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot
from IPython.core.pylabtools import figsize
from sklearn.model_selection import train_test_split, validation_curve, learning_curve
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from sklearn.neighbors import KNeighborsRegressor
import warnings

warnings.filterwarnings('ignore')
pyplot.rcParams['font.size'] = 22
sns.set(font_scale=2)


# Вычисление средней абсолютной ошибки
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))


# Вычисление средней 'относительной' ошибки
def mre(y_true, y_pred):
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# Обучение и тестирование модели
def fit_and_evaluate(model):
    model.fit(X, y)

    model_pred = model.predict(X_test)
    model_mae_and_mre = (mae(y_test, model_pred), mre(y_test, model_pred))

    return model_mae_and_mre


# Таблица отсутствующих значений в процентном соотношении
def missing_values_table(df):
    mis_val = df.isnull().sum()
    mis_val_percent = 100 * df.isnull().sum() / len(df)
    mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
    mis_val_table_ren_columns = mis_val_table.rename(
        columns={0: 'Отсутствующие значения', 1: '% от всех значений'})
    mis_val_table_ren_columns = mis_val_table_ren_columns[
        mis_val_table_ren_columns.iloc[:, 1] != 0].sort_values(
        '% от всех значений', ascending=False).round(1)
    print("В data frame содержится " + str(df.shape[1]) + " столбцов.\n"
                                                          "Всего " + str(
        mis_val_table_ren_columns.shape[0]) +
          " столбец с отсутствующими значениями.")
    return mis_val_table_ren_columns


def plot_learning_curve(estimator, title, X, y, axes=None, ylim=None, cv=None,
                        n_jobs=None, train_sizes=np.linspace(.1, 1.0, 5)):
        if axes is None:
        _, axes = pyplot.subplots(1, 1, figsize=(20, 5))

    axes.set_title(title)
    if ylim is not None:
        axes.set_ylim(*ylim)
    axes.set_xlabel("Обучающие примеры")
    axes.set_ylabel("Значение")

    train_sizes, train_scores, test_scores, fit_times, _ = \
        learning_curve(estimator, X, y, cv=cv, n_jobs=n_jobs,
                       train_sizes=train_sizes,
                       return_times=True)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    axes.grid()
    axes.fill_between(train_sizes, train_scores_mean - train_scores_std,
                         train_scores_mean + train_scores_std, alpha=0.1,
                         color="r")
    axes.fill_between(train_sizes, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.1,
                         color="g")
    axes.plot(train_sizes, train_scores_mean, 'o-', color="r",
                 label="Значение обучения")
    axes.plot(train_sizes, test_scores_mean, 'o-', color="g",
                 label="Значение кросс-валидации")
    axes.legend(loc="best")


path_less = "D:\\Лабы по машинному обучению\\Лаба4\\5\\HOUSE_PRICES_LESS.csv"

data_less = pd.read_csv(path_less, delimiter=",")

# Процент пропущенных значений для каждого признака
missing_df = missing_values_table(data_less)
print(missing_df)

missing_columns = list(missing_df[missing_df['% от всех значений'] >= 30].index)
data = data_less.drop(columns=list(missing_columns))
print(f'Столбцы, которые нужно убрать из выборки: {missing_columns}')

# Замена Not Available на not a number
data = data.replace({'Not Available': np.nan})

# Первичный анализ признаков
print(f'Первичный анализ: {data.describe()}')

print(f'Медиана: \n{data.median()}\nМода: \n{data.mode()}')

# Все корреляции с целевым параметром
correlations_data = data.corr()['Price'].sort_values()
print(f'Корреляции с целевым параметром: \n{correlations_data}')

# Первичный визуальный анализ признаков

# График плотности Price, раскрашенный в зависимости от типа
# types = data.dropna(subset=['Price'])
# types = types['Type'].value_counts()
# types = list(types.index)
#
# for t in types:
#     subset = data[data['Type'] == t]
#     sns.kdeplot(subset['Price'].dropna(),
#                 label=t, fill=False, alpha=0.8)
# figsize(12, 10)
# pyplot.xlabel('Цена', size=20)
# pyplot.ylabel('Плотность', size=20)
# pyplot.title('График плотности целевого параметра Price для каждого типа здания', size=28)
# pyplot.legend(loc="best")
# pyplot.show()

# График плотности Price, раскрашенный в зависимости от региона
# rns = data.dropna(subset=['Price'])
# rns = rns['Regionname'].value_counts()
# rns = list(rns.index)
#
# for r in rns:
#     subset = data[data['Regionname'] == r]
#     sns.kdeplot(subset['Price'].dropna(),
#                 label=r, fill=False, alpha=0.8)
#
# figsize(12, 10)
# pyplot.xlabel('Цена', size=20)
# pyplot.ylabel('Плотность', size=20)
# pyplot.title('График плотности целевого параметра Price для каждого региона', size=28)
# pyplot.legend(loc="best")
# pyplot.show()

# Гитограмма для параметра Type
# data['Type'].hist()
# pyplot.show()


# Топ 10 продавцов
# values, bins, patches = pyplot.hist(data['SellerG'], bins=1000)
# order = np.argsort(values)[::-1]
# for i in order[:10]:
#     patches[i].set_color('fuchsia')

# Зависимость стоимости от количества комнат
# pyplot.scatter(data['Price'], data['Rooms'])

# Зависимость стоимости от региона
# pyplot.scatter(data['Regionname'], data['Price'])

# Диаграмма размаха цены здания в зависимости от типа
# data.boxplot(column=['Price'], by=['Type'], grid=False)

# Топ 10 продавцов в виде круговой диаграммы
# data.groupby(['SellerG']).size().sort_values().tail(10).plot(kind='pie', autopct='%1.0f%%')
# pyplot.show()

# Преобразование данных
features = data.copy()
numeric_subset = data.select_dtypes('number')

for col in numeric_subset.columns:
    if col == 'Price':
        next
    else:
        numeric_subset['log_' + col] = np.log(numeric_subset[col])

categorical_subset = data[['Type', 'Regionname']]
categorical_subset = pd.get_dummies(categorical_subset)

features = pd.concat([numeric_subset, categorical_subset], axis=1)

# Разделение на обучающую и тестовую выборки
price = features[features['Price'].notnull()]

features = price.drop(columns='Price')
targets = pd.DataFrame(price['Price'])

features = features.replace({np.inf: np.nan, -np.inf: np.nan})

X, X_test, y, y_test = train_test_split(features, targets, test_size=0.25, random_state=42)

baseline_guess = np.median(y)

print(
    f'Средняя абсолютная ошибка на тестовом наборе = {mae(y_test, baseline_guess).item()}, что в процентном соотношении = '
    f'{mre(y_test, baseline_guess).item()}%')

# Заполнение отсутствующих значений
imputer = SimpleImputer(strategy='median')

imputer.fit(X)

X = imputer.transform(X)
X_test = imputer.transform(X_test)

# Масштабирование значений
scaler = MinMaxScaler(feature_range=(0, 1))

scaler.fit(X)

X = scaler.transform(X)
X_test = scaler.transform(X_test)

y = np.array(y).reshape((-1,))
y_test = np.array(y_test).reshape((-1,))

print('Количество отсутствующих значений в наборе для обучения: ', np.sum(np.isnan(X)))
print('Количество отсутствующих значений в наборе для тестирования:  ', np.sum(np.isnan(X_test)))

# Модель
knn = KNeighborsRegressor(n_neighbors=10)
knn_mae = fit_and_evaluate(knn)

print(f'MAE для модели kNN = {knn_mae[0].item()}, что в процентном соотношении = {knn_mae[1].item()}%')

pyplot.style.use('fivethirtyeight')
figsize(8, 6)

# Прогноз
pred = knn.predict(X_test)
print(f'Прогноз: \n{pred}\nЗначения: \n{y_test}')
sns.kdeplot(pred, label='Прогноз')
sns.kdeplot(y_test, label='Значения')

pyplot.xlabel('Цена')
pyplot.ylabel('Плотность')
pyplot.title('Значения и прогноз')
pyplot.legend(loc="best")
pyplot.show()

# Построение кривой обучения
title = "Кривая обучения"
estimator = KNeighborsRegressor(n_neighbors=10)
plot_learning_curve(estimator, title, X, y)
pyplot.show()


# Построение кривой валидации
param_range = [5, 10, 12, 15, 18, 20]
train_scores, test_scores = validation_curve(KNeighborsRegressor(), X, y,
                                             param_name="n_neighbors", param_range=param_range)

train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)

pyplot.title("Кривая валидации")
pyplot.xlabel("n_neighbors")
pyplot.ylabel("Значение")
lw = 2
pyplot.semilogx(param_range, train_scores_mean, label="Training score",
             color="darkorange", lw=lw)
pyplot.fill_between(param_range, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.2,
                 color="darkorange", lw=lw)
pyplot.semilogx(param_range, test_scores_mean, label="Cross-validation score",
             color="navy", lw=lw)
pyplot.fill_between(param_range, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.2,
                 color="navy", lw=lw)
pyplot.legend(loc="best")
pyplot.show()
