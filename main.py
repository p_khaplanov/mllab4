from scipy.stats import randint as sp_randInt
import xgboost as xgb
import pandas as pd
import numpy as np
import math
from sklearn.metrics import mean_absolute_error, r2_score, median_absolute_error
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
from sklearn.model_selection import train_test_split, validation_curve, learning_curve
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot
from IPython.core.pylabtools import figsize
from sklearn.model_selection import train_test_split, validation_curve, learning_curve
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from sklearn.neighbors import KNeighborsRegressor
import warnings

#Вычисляет среднюю абсолютную процентную ошибку
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

#Вычисляет медианную абсолютную процентную ошибку
def median_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.median(np.abs((y_true - y_pred) / y_true)) * 100

#Печатает рассчитанные значения коэффициента детерминации, средней и медианной абсолютных ошибок
def print_metrics(prediction, val_y):
    val_mae = mean_absolute_error(val_y, prediction)
    median_AE = median_absolute_error(val_y, prediction)
    r2 = r2_score(val_y, prediction)
    print('')
    print('R\u00b2: {:.2}'.format(r2))
    print('')
    print('Средняя абсолютная ошибка: {:.3} %'.format(mean_absolute_percentage_error(val_y, prediction)))
    print('Медианная абсолютная ошибка: {:.3} %'.format(median_absolute_percentage_error(val_y, prediction)))
#процент nulloвых строк в каждом признаке
def print_null_val(df):
    for col in df.columns:
        pct_missing = np.mean(df[col].isnull())
        print('{} - {}%'.format(col, round(pct_missing * 100)))

def print_info_dataset(df):
    print('Размер Dataframe:', df.shape)
    print('\n\nИнформация о Dataframe df.info():')
    print(df.info())
#Переводим дату в дни от Рождества Христова
def date_to_days(dates):
    Days = []
    for date in dates: #16/12/2017 1/04/2017
        if (len(date) == 10):
            days = date[0:2]
            months = date[3:5]
            years = date[6:10]
            # для упрощения в месяце 30 дней, в году 365 дней
            totalDays = int(days) + int(months) * 30 + int(years) * 365
        else:
            days = date[0:1]
            months = date[2:4]
            years = date[5:9]
            # для упрощения: в месяце 30 дней, в году 365 дней
            totalDays = int(days) + int(months) * 30 + int(years) * 365
        Days.append(totalDays)
    return Days
#Функция показывает сколько уникальных элементов в каждом признаке
def unique_signs(df):
    unique_counts = pd.DataFrame.from_records([(col, df[col].nunique()) for col in df.columns],
                                              columns=['Column_Name', 'Num_Unique']).sort_values(by=['Num_Unique'])
    print(unique_counts)


def plot_learning_curve(estimator, title, X, y, axes=None, ylim=None, cv=None,
                        n_jobs=None, train_sizes=np.linspace(.1, 1.0, 5)):
    if axes is None:
        _, axes = pyplot.subplots(1, 1, figsize=(20, 5))

    axes.set_title(title)
    if ylim is not None:
        axes.set_ylim(*ylim)
    axes.set_xlabel("Обучающие примеры")
    axes.set_ylabel("Значение")

    train_sizes, train_scores, test_scores, fit_times, _ = \
        learning_curve(estimator, X, y, cv=cv, n_jobs=n_jobs,
                       train_sizes=train_sizes,
                       return_times=True)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    axes.grid()
    axes.fill_between(train_sizes, train_scores_mean - train_scores_std,
                      train_scores_mean + train_scores_std, alpha=0.1,
                      color="r")
    axes.fill_between(train_sizes, test_scores_mean - test_scores_std,
                      test_scores_mean + test_scores_std, alpha=0.1,
                      color="g")
    axes.plot(train_sizes, train_scores_mean, 'o-', color="r",
              label="Значение обучения")
    axes.plot(train_sizes, test_scores_mean, 'o-', color="g",
              label="Значение кросс-валидации")
    axes.legend(loc="best")


#Первичный анализ признаков
def primary_analysis(df):
    print(f'Первичный анализ:')
    print(df.describe(include="all"))
    print(f'Медиана: \n{df.median()}\nМода: \n{df.mode()}')
    # Все корреляции с целевым параметром
    correlations_data= df.corr()['Price'].sort_values()
    print(f'Корреляции с целевым параметром: \n{correlations_data}')

#Первичный анализ признаков
def primary_visual_analysis(df):
    #График плотности Price, раскрашенный в зависимости от типа
    types = df.dropna(subset=['Price'])
    types = types['Type'].value_counts()
    types = list(types.index)

    for t in types:
        subset = df[df['Type'] == t]
        sns.kdeplot(subset['Price'].dropna(),
                    label=t, fill=False, alpha=0.8)
    figsize(12, 10)
    pyplot.xlabel('Цена', size=20)
    pyplot.ylabel('Плотность', size=20)
    pyplot.title('График плотности целевого параметра Price для каждого типа здания', size=28)
    pyplot.legend(loc="best")
    pyplot.show()

    #График плотности Price, раскрашенный в зависимости от региона
    rns = df.dropna(subset=['Price'])
    rns = rns['Regionname'].value_counts()
    rns = list(rns.index)

    for r in rns:
        subset = df[df['Regionname'] == r]
        sns.kdeplot(subset['Price'].dropna(),
                    label=r, fill=False, alpha=0.8)

    figsize(12, 10)
    pyplot.xlabel('Цена', size=20)
    pyplot.ylabel('Плотность', size=20)
    pyplot.title('График плотности целевого параметра Price для каждого региона', size=28)
    pyplot.legend(loc="best")
    pyplot.show()

    #Гитограмма для параметра Type
    df['Type'].hist()
    pyplot.show()


    #Топ 10 продавцов
    values, bins, patches = pyplot.hist(df['SellerG'], bins=1000)
    order = np.argsort(values)[::-1]
    for i in order[:10]:
        patches[i].set_color('fuchsia')

    #Зависимость стоимости от количества комнат
    pyplot.scatter(df['Price'], df['Rooms'])

    #Зависимость стоимости от региона
    #pyplot.scatter(df['Regionname'], df['Price'])

    #Диаграмма размаха цены здания в зависимости от типа
    df.boxplot(column=['Price'], by=['Type'], grid=False)

    #Топ 10 продавцов в виде круговой диаграммы
    df.groupby(['SellerG']).size().sort_values().tail(10).plot(kind='pie', autopct='%1.0f%%')
    pyplot.show()


#Загрузка данных
df = pd.read_csv("data/housing_FULL.csv")
print(df.nunique().sort_values())
#Добавление столбца "дни"
df['Days'] = date_to_days(df['Date'])
#Первичная информация о признаках датасета, его налловые значения
print_info_dataset(df)
print_null_val(df)
primary_analysis(df)
#Первичный визуальны анализ
#primary_visual_analysis(df)
#Выбросы
first_quartile = df.quantile(q=0.25)
third_quartile = df.quantile(q=0.75)
IQR = third_quartile - first_quartile
outliers = df[(df > (third_quartile + 1.5 * IQR)) | (df < (first_quartile - 1.5 * IQR))].count(axis=1)
outliers.sort_values(axis=0, ascending=False, inplace=True)
# Удаляем из датафрейма 3000 строк, подходящих под критерии выбросов
outliers = outliers.head(3000)
df.drop(outliers.index, inplace=True)
#Замена пропущенных значений в столбце Прайс на среднее арифметическое
#df['Price'].fillna(df['Price'].mean(), inplace = True)
#Удаление пропущенных значений в столбце Прайс
df.dropna(subset=['Price'], inplace=True)
cat_columns = df.select_dtypes(['object']).columns
df[cat_columns] = df[cat_columns].apply ( lambda x: pd.factorize (x)[ 0 ])

y = df['Price']
#Создаем список признаков, на основании которых будем строить модели
features = [
            'Suburb',
            'Address',
            'Rooms',
            'Type',
            'Method',
            'SellerG',
            'Date',
            'Distance',
            'Postcode',
            #'Bedroom2',
            'Bathroom',
            #'Car',
            'Landsize',
            #'BuildingArea',
            #'YearBuilt',
            'CouncilArea',
            'Lattitude',
            'Longtitude',
            #'Regionname',
            'Propertycount',
            'Days'
           ]
for feature in features:
    df[feature].fillna(df[feature].median(), inplace=True)

primary_analysis(df)

print_info_dataset(df)
print_null_val(df)
#Создаем датафрейм, состоящий из признаков, выбранных ранее
X = df[features]
#Проводим случайное разбиение данных на выборки для обучения (train) и валидации (val), по умолчанию в пропорции 0.9/0.1
train_X, val_X, train_y, val_y = train_test_split(X, y,test_size=0.1, random_state=1)

rf_model = RandomForestRegressor(n_estimators=100,
                                 n_jobs=-4,
                                 bootstrap=True,
                                 criterion='absolute_error',
                                 max_features= 'auto',
                                 random_state=6,
                                 max_depth=56,
                                 min_samples_split=5
                                 )
"""
parametrs = {
               'bootstrap': [True, False],
               'max_depth': sp_randInt(1,100),
                'n_jobs' : sp_randInt(1,100),
               'max_features': ['auto', 'sqrt'],
               'min_samples_split': sp_randInt(1,100),
               'n_estimators': [130, 180, 230,600,1000,2000,5000],
                'random_state': sp_randInt(1,10)}
model = RandomForestRegressor()
random_src = RandomizedSearchCV(estimator = model,param_distributions = parametrs,
                               cv = 4, n_iter = 25, n_jobs=-1)
random_src.fit(train_X, train_y)
print(" Results from Random Search " )
print("\n The best estimator across ALL searched params:\n", random_src.best_estimator_)
print("\n The best score across ALL searched params:\n", random_src.best_score_)
print("\n The best parameters across ALL searched params:\n", random_src.best_params_)
rf_prediction = random_src.predict(val_X).round(0)
"""


#Проводим подгонку модели на обучающей выборке
rf_model.fit(train_X, train_y)

#Вычисляем предсказанные значения цен на основе валидационной выборки
rf_prediction = rf_model.predict(val_X).round(0)
"""
#Вычисляем и печатаем величины ошибок при сравнении известных цен квартир из валидационной выборки с предсказанными моделью
print_metrics(rf_prediction, val_y)
print(f'Прогноз: \n{rf_prediction}\nЗначения: \n{val_y}')
sns.kdeplot(rf_prediction, label='Прогноз')
sns.kdeplot(val_y, label='Значения')

pyplot.xlabel('Цена')
pyplot.ylabel('Плотность')
pyplot.title('Значения и прогноз')
pyplot.legend(loc="best")
pyplot.show()

#Рассчитываем важность признаков в модели Random forest
importances = rf_model.feature_importances_
std = np.std([tree.feature_importances_ for tree in rf_model.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

#Печатаем рейтинг признаков
print("Рейтинг важности признаков:")
for f in range(X.shape[1]):
    print("%d. %s (%f)" % (f + 1, features[indices[f]], importances[indices[f]]))

#Строим столбчатую диаграмму важности признаков
plt.figure()
plt.title("Важность признаков")
plt.bar(range(X.shape[1]), importances[indices], color="g", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()
title = "Кривая обучения"
plot_learning_curve(rf_model, title, X, y)
pyplot.show()
"""
# Построение кривой валидации
param_range = [10, 50, 100, 200]
train_scores, test_scores = validation_curve(RandomForestRegressor(
                                 ), X, y,
                                             param_name="n_estimators", param_range=param_range)

train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)

pyplot.title("Кривая валидации")
pyplot.xlabel("n_neighbors")
pyplot.ylabel("Значение")
lw = 2
pyplot.semilogx(param_range, train_scores_mean, label="Training score",
             color="darkorange", lw=lw)
pyplot.fill_between(param_range, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.2,
                 color="darkorange", lw=lw)
pyplot.semilogx(param_range, test_scores_mean, label="Cross-validation score",
             color="navy", lw=lw)
pyplot.fill_between(param_range, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.2,
                 color="navy", lw=lw)
pyplot.legend(loc="best")
pyplot.show()
